export enum EntityTypesEnum {
  Student = 'student',
  Group = 'group',
}

export interface IUserInfo {
  id: number;
  name: string;
  middle_name: string;
  last_name: string;
  email: string;
  vstu_id: string;
}

export interface IGroupStudent {
  id: number;
  name: string;
  login: string;
  vstu_id: string;
  success_count_tasks: number;
  count_tasks: number;
  adding_score_tasks: number;
  score_tasks: number;
}

export interface IAllocateRequestData {
  rating: number,
  count: number,
}
