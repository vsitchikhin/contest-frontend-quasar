import { defineStore } from 'pinia';
import {
  IAdminTaskDto,
  IGroupTaskDto,
  ITaskDto,
  ITaskShortDto,
  IUserTaskDto,
} from 'src/modules/tasks/types/tasks.types';
import { LoadingStatusActionsEnum, LoadingStatusCodesEnum, TLoadingStatus } from 'src/types/base.types';

interface ITasksState {
  taskList: ITaskDto[] | null;
  task: ITaskDto | null;

  adminTaskList: IAdminTaskDto[] | null;

  studentTaskList: IUserTaskDto[] | null;
  groupTaskList: IGroupTaskDto[] | null;

  taskListLoadingStatus: TLoadingStatus;
  taskLoadingStatus: TLoadingStatus;

  adminTaskLoadingStatus: TLoadingStatus;

  studentTaskListLoadingStatus: TLoadingStatus;
  groupTaskListLoadingStatus: TLoadingStatus;

  adminUserTaskList: ITaskShortDto[] | null;

  adminUserTaskListLoadingStatus: TLoadingStatus;

  adminTaskHistory: ITaskDto | null;

  adminTaskHistoryLoadingStatus: TLoadingStatus;
}

export const tasksStore = defineStore({
  id: 'tasks',

  state: (): ITasksState => ({
    taskList: null,
    task: null,

    adminTaskList: null,

    studentTaskList: null,
    groupTaskList: null,

    adminUserTaskList: null,

    adminTaskHistory: null,

    taskListLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    taskLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    adminTaskLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    studentTaskListLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    groupTaskListLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    adminUserTaskListLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    adminTaskHistoryLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },
  }),

  actions: {
    // ----------------------------------------------------------------
    // Методы обновления данных
    SET_TASK_PAYLOAD(payload: ITaskDto | null) {
      this.task = payload;
    },

    SET_TASK_LIST_PAYLOAD(payload: ITaskDto[] | null) {
      this.taskList = payload;
    },

    SET_ADMIN_TASK_LIST_PAYLOAD(payload: IAdminTaskDto[] | null) {
      this.adminTaskList = payload;
    },

    SET_STUDENT_TASK_LIST_PAYLOAD(payload: IUserTaskDto[] | null) {
      this.studentTaskList = payload;
    },

    SET_GROUP_TASK_LIST_PAYLOAD(payload: IGroupTaskDto[] | null) {
      this.groupTaskList = payload;
    },

    SET_ADMIN_USER_TASK_LIST_PAYLOAD(payload: ITaskShortDto[] | null) {
      this.adminUserTaskList = payload;
    },

    SET_ADMIN_TASK_HISTORY_PAYLOAD(payload: ITaskDto | null) {
      this.adminTaskHistory = payload;
    },

    // ----------------------------------------------------------------
    // Методы установки лоадинг-статусов
    SET_TASK_LOADING_STATUS(status: TLoadingStatus) {
      this.taskLoadingStatus = {
        ...this.taskLoadingStatus,
        ...status,
      };
    },

    SET_TASK_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.taskListLoadingStatus = {
        ...this.taskListLoadingStatus,
        ...status,
      };
    },

    SET_ADMIN_TASK_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.adminTaskLoadingStatus = {
        ...this.adminTaskLoadingStatus,
        ...status,
      };
    },

    SET_STUDENT_TASK_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.studentTaskListLoadingStatus = {
        ...this.studentTaskListLoadingStatus,
        ...status,
      };
    },

    SET_GROUP_TASK_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.groupTaskListLoadingStatus = {
        ...this.groupTaskListLoadingStatus,
        ...status,
      };
    },

    SET_ADMIN_USER_TASK_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.adminUserTaskListLoadingStatus = {
        ...this.adminUserTaskListLoadingStatus,
        ...status,
      };
    },

    SET_ADMIN_TASK_HISTORY_LOADING_STATUS(status: TLoadingStatus) {
      this.adminTaskHistoryLoadingStatus = {
        ...this.adminTaskHistoryLoadingStatus,
        ...status,
      };
    },
  },
});
