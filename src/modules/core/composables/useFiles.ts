export async function useFileParser(file: File | null): Promise<string | void> {
  if (!file) {
    return;
  }

  return JSON.parse(await file.slice().text());
}

export function useFileDownloader(url: string, fileName: string) {
  const a = document.createElement('a');
  a.href = url;
  a.download = `${fileName}`;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}
