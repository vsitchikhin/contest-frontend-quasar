import { coursesStore } from 'src/modules/courses/services/courses.store';
import { Service } from 'src/modules/service';
import { IAdminCourseDto, ICourseShortDto } from 'src/modules/courses/types/courses.types';
import { LoadingStatusActionsEnum, LoadingStatusCodesEnum, TLoadingStatus } from 'src/types/base.types';
import { api } from 'boot/axios';
import { EntityTypesEnum, IAllocateRequestData, IGroupStudent, IUserInfo } from 'src/modules/courses/types/entity.types';
import { IGroupDto } from 'src/modules/groups/types/groups.types';
import { useFileDownloader, useFileParser } from 'src/modules/core/composables/useFiles';

export class CoursesService extends Service {
  private store;

  public constructor() {
    super();
    this.store = coursesStore();
  }

  // ------------------------------------------------------------------
  // Геттеры
  public get courses(): ICourseShortDto[] | null {
    return this.store.courses;
  }

  public get course(): ICourseShortDto | null {
    return this.store.course;
  }

  public get coursesLoadingStatus(): TLoadingStatus {
    return this.store.coursesLoadingStatus;
  }

  public get courseLoadingStatus(): TLoadingStatus {
    return this.store.courseLoadingStatus;
  }

  public get adminCourses(): IAdminCourseDto[] | null {
    return this.store.adminCourses;
  }

  public get adminCoursesLoadingStatus(): TLoadingStatus {
    return this.store.adminCoursesLoadingStatus;
  }

  public get courseStudents(): IUserInfo[] | null {
    return this.store.courseStudents;
  }

  public get courseStudentsLoadingStatus(): TLoadingStatus {
    return this.store.courseStudentsLoadingStatus;
  }

  public get courseGroups(): IGroupDto[] | null {
    return this.store.courseGroups;
  }

  public get courseGroupsLoadingStatus(): TLoadingStatus {
    return this.store.courseGroupsLoadingStatus;
  }

  public get adminGroupData(): IGroupStudent[] | null {
    return this.store.adminGroupData;
  }

  public get adminGroupDataLoadingStatus(): TLoadingStatus {
    return this.store.adminGroupDataLoadingStatus;
  }

  public get outGroupList(): IGroupDto[] | null {
    return this.store.outGroupList;
  }

  public get outGroupListLoadingStatus(): TLoadingStatus {
    return this.store.outGroupListLoadingStatus;
  }

  public get allocateLoadingStatus(): TLoadingStatus {
    return this.store.allocateLoadingStatus;
  }

  public get createTasksLoadingStatus(): TLoadingStatus {
    return this.store.createTasksLoadingStatus;
  }

  public get patchTaskLoadingStatus(): TLoadingStatus {
    return this.store.patchTaskLoadingStatus;
  }

  public get createCourseLoadingStatus(): TLoadingStatus {
    return this.store.createCourseLoadingStatus;
  }

  // ------------------------------------------------------------------
  // API запросы
  public async loadCourses(): Promise<boolean> {
    // const tokenPlugin = <ITokenPlugin>useNuxtApp().$token;
    this.store.SET_COURSES_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get('/api/courses', {
        headers: {
          ...this.apiHeaders,
        },
      });

      this.store.SET_COURSES_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      this.store.SET_COURSES_PAYLOAD(response.data);

      return true;
    } catch(e: any) {
      console.log(e);
      this.store.SET_COURSES_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async loadAdminCourses(): Promise<boolean> {
    this.store.SET_ADMIN_COURSES_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get('/api/admin/courses', {
        headers: {
          ...this.apiHeaders,
        },
      });

      this.store.SET_ADMIN_COURSES_PAYLOAD(response.data);
      this.store.SET_ADMIN_COURSES_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      console.log(e);
      this.store.SET_ADMIN_COURSES_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async loadCourseStudents(courseId: string) {
    this.store.SET_COURSE_STUDENTS_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get(`/api/admin/courses/${courseId}/users`, {
        headers: {
          ...this.apiHeaders,
        },
      });

      this.store.SET_COURSE_STUDENTS_PAYLOAD(response.data);
      this.store.SET_COURSE_STUDENTS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch (e: any) {
      console.log(e);
      this.store.SET_COURSE_STUDENTS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async loadCourseGroups(courseId: string) {
    this.store.SET_COURSE_GROUPS_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get(`/api/admin/courses/${courseId}/groups`, {
        headers: {
          ...this.apiHeaders,
        },
      });

      this.store.SET_COURSE_GROUPS_PAYLOAD(response.data);
      this.store.SET_COURSE_GROUPS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch (e: any) {
      console.log(e);
      this.store.SET_COURSE_GROUPS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async loadAdminGroupData(groupId: string, courseId: string) {
    this.store.SET_ADMIN_GROUP_DATA_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get(`/api/admin/groups/${groupId}/courses/${courseId}/users`, {
        headers: { ...this.apiHeaders },
      });

      this.store.SET_ADMIN_GROUP_DATA_PAYLOAD(response.data);
      this.store.SET_ADMIN_GROUP_DATA_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e:any) {
      this.store.SET_ADMIN_GROUP_DATA_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async loadOutGroups(courseId: string) {
    this.store.SET_OUT_GROUP_LIST_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get(`/api/admin/courses/${courseId}/groups/out`, {
        headers: { ...this.apiHeaders },
      });

      this.store.SET_OUT_GROUP_LIST_PAYLOAD(response.data);
      this.store.SET_OUT_GROUP_LIST_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      this.store.SET_OUT_GROUP_LIST_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return true;
    }
  }

  public async loadTaskFile(taskId: string) {
    this.store.SET_TASK_FILE_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get(`/api/admin/tasks/${taskId}`, {
        headers: {
          ...this.apiHeaders,
          'Content-Type': 'text/json',
        },
        responseType: 'blob',
      });

      const url = URL.createObjectURL(response.data);
      const fileName = `task_${taskId}.json`;

      useFileDownloader(url, fileName);
      this.store.SET_TASK_FILE_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      this.store.SET_TASK_FILE_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async createTasks(courseId: string, file: File) {
    this.store.SET_CREATE_TASKS_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const tasks = await useFileParser(file);

      await api.post(`/api/admin/tasks/courses/${courseId}/format`, tasks, {
        headers: {
          ...this.apiHeaders,
          'Content-Type': 'application/json',
        },
      });

      this.store.SET_CREATE_TASKS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      this.store.SET_CREATE_TASKS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async patchTask(courseId: string, taskId: string, file: File) {
    this.store.SET_PATCH_TASK_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const task = await useFileParser(file);

      await api.put(`/api/admin/tasks/${taskId}`, task, {
        headers: {
          ...this.apiHeaders,
          'Content-Type': 'application/json',
        },
      });

      this.store.SET_PATCH_TASK_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });
      return true;
    } catch(e: any) {
      this.store.SET_PATCH_TASK_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async createCourse(name: string, file: File | null) {
    this.store.SET_CREATE_COURSE_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const tasks = await useFileParser(file);

      await api.post('/api/admin/courses/format', {
        course_name: name,
        tasks: tasks,
      }, {
        headers: {
          ...this.apiHeaders,
          'Content-Type': 'application/json',
        },
      });

      this.store.SET_CREATE_COURSE_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      this.store.SET_CREATE_COURSE_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async getCourseCSV(courseId: string, groupId: string, groupName: string) {
    this.store.SET_CSV_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get(`/api/admin/groups/${groupId}/courses/${courseId}/stat`, {
        headers: {
          ...this.apiHeaders,
          'Content-Type': 'text/csv',
        },
        responseType: 'blob',
      });

      const url = URL.createObjectURL(response.data);
      const fileName = `Stat_group_${groupName}`;

      useFileDownloader(url, fileName);

      this.store.SET_CSV_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });
    } catch(err: any) {
      this.store.SET_CSV_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: err.statusCode?.toString(),
        msg: err.message,
      });
    }
  }

  // ------------------------------------------------------------------
  // Методы
  public setCourse(courseId: string) {
    const course: ICourseShortDto | undefined = this.store.courses?.find(course => course.id.toString() === courseId);

    this.store.SET_COURSE_PAYLOAD(course || null);
  }

  public async loadEntityList(entityType: EntityTypesEnum, courseId: string) {
    if (entityType === EntityTypesEnum.Student) {
      await this.loadCourseStudents(courseId);
    } else if (entityType === EntityTypesEnum.Group) {
      await this.loadCourseGroups(courseId);
    }
  }

  public async deleteGroup(groupId: string, courseId: string) {
    this.store.SET_COURSE_GROUPS_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.deleting,
    });

    try {
      await api.delete(`/api/admin/groups/${groupId}/courses/${courseId}`, {
        headers: { ...this.apiHeaders },
      });

      const result = await this.loadCourseGroups(courseId);
      return result;

    } catch (e: any) {
      this.store.SET_COURSE_GROUPS_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async allocateTasks(groupId: string, courseId: string, data: IAllocateRequestData[]) {
    this.store.SET_ALLOCATE_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.saving,
    });

    try {
      await api.post(`/api/admin/groups/${groupId}/courses/${courseId}/random`, data, {
        headers: { ...this.apiHeaders },
      });

      this.store.SET_ALLOCATE_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      this.store.SET_ALLOCATE_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }
}
