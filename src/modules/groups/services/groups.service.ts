import { Service } from 'src/modules/service';
import { groupsStore } from 'src/modules/groups/services/groups.store';
import { ICreateGroupDto, IGroupDto } from 'src/modules/groups/types/groups.types';
import { LoadingStatusActionsEnum, LoadingStatusCodesEnum, TLoadingStatus } from 'src/types/base.types';
import { api } from 'boot/axios';
import CyrillicToTranslit from 'cyrillic-to-translit-js';
import { useFileDownloader } from 'src/modules/core/composables/useFiles';

export class GroupsService extends Service {
  private store;

  public constructor() {
    super();
    this.store = groupsStore();
  }

  // -----------------------------------------------------------------
  // Геттеры
  public get groupList(): IGroupDto[] | null {
    return this.store.groupList;
  }

  public get groupListLoadingStatus(): TLoadingStatus {
    return this.store.groupListLoadingStatus;
  }

  public get CSVLoadingStatus(): TLoadingStatus {
    return this.store.getCSVLoadingStatus;
  }

  public get CSVLoadingGroupID(): number | string | null {
    return this.store.CSVLoadingGroupId;
  }

  public get createGroupLoadingStatus(): TLoadingStatus {
    return this.store.createGroupLoadingStatus;
  }

  // -----------------------------------------------------------------
  // API запросы
  public async loadGroupList() {
    this.store.SET_GROUP_LIST_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      const response = await api.get('/api/admin/groups', {
        headers: { ...this.apiHeaders },
      });

      this.store.SET_GROUP_LIST_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });
      this.store.SET_GROUP_LIST_PAYLOAD(response.data);

      return true;
    } catch(e: any) {
      this.store.SET_GROUP_LIST_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  public async createGroup(group: ICreateGroupDto) {

    this.store.SET_CREATE_GROUP_LOADING_STATUS({
      code: LoadingStatusCodesEnum.loaded,
      action: LoadingStatusActionsEnum.saving,
    });

    try {
      await api.post('/api/admin/groups', group, {
        headers: { ...this.apiHeaders },
      });

      this.store.SET_CREATE_GROUP_LOADING_STATUS({
        code: LoadingStatusCodesEnum.notLoaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return true;
    } catch(e: any) {
      this.store.SET_CREATE_GROUP_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode.toString(),
        msg: e.errorMessage,
      });
      return false;
    }
  }

  public async getGroupCSV(groupId: string, groupName: string) {
    groupName = groupName.replace(/ /g,'_');

    this.store.SET_CSV_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    this.store.SET_LOADING_CSV_GROUP_ID(groupId);

    try {
      const response = await api.get(`/api/admin/groups/${groupId}/exportUsers`, {
        headers: {
          ...this.apiHeaders,
          'Content-Type': 'text/csv',
        },
        responseType: 'blob',
      });

      const url = URL.createObjectURL(response.data);
      // @ts-ignore
      const fileName = `${new CyrillicToTranslit().transform(groupName).toLowerCase()}.csv`;

      useFileDownloader(url, fileName);

      // this.store.SET_CSV_FILE(response.data);
      this.store.SET_CSV_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });
      this.store.SET_LOADING_CSV_GROUP_ID(null);

      return true;
    } catch(e:any) {
      console.log(e);
      this.store.SET_CSV_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });
      this.store.SET_LOADING_CSV_GROUP_ID(null);
      return false;
    }
  }

  public async deleteGroup(groupId: string) {
    this.store.SET_GROUP_LIST_LOADING_STATUS({
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.loading,
    });

    try {
      await api.delete(`/api/admin/groups/${groupId}`, {
        headers: { ...this.apiHeaders },
      });

      const result = await this.loadGroupList();

      this.store.SET_GROUP_LIST_LOADING_STATUS({
        code: LoadingStatusCodesEnum.loaded,
        action: LoadingStatusActionsEnum.noAction,
      });

      return result;
    } catch(e: any) {
      this.store.SET_CREATE_GROUP_LOADING_STATUS({
        code: LoadingStatusCodesEnum.error,
        action: LoadingStatusActionsEnum.noAction,
        errorCode: e.statusCode?.toString(),
        msg: e.errorMessage,
      });

      return false;
    }
  }

  // -----------------------------------------------------------------
  // Методы
}
