import { defineStore } from 'pinia';
import { IGroupDto } from 'src/modules/groups/types/groups.types';
import { LoadingStatusActionsEnum, LoadingStatusCodesEnum, TLoadingStatus } from 'src/types/base.types';

interface GroupState {
  groupList: IGroupDto[] | null;
  groupListLoadingStatus: TLoadingStatus;

  CSVLoadingGroupId: number | string | null;
  csvFile: string | null;

  createGroupLoadingStatus: TLoadingStatus;

  getCSVLoadingStatus: TLoadingStatus;
}

export const groupsStore = defineStore({
  id: 'groups',

  state: (): GroupState => ({
    groupList: null,
    groupListLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    createGroupLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    CSVLoadingGroupId: null,
    csvFile: null,

    getCSVLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },
  }),

  actions: {
    SET_GROUP_LIST_PAYLOAD(payload: IGroupDto[] | null) {
      this.groupList = payload;
    },

    SET_GROUP_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.groupListLoadingStatus = {
        ...this.groupListLoadingStatus,
        ...status,
      };
    },

    SET_CREATE_GROUP_LOADING_STATUS(status: TLoadingStatus) {
      this.createGroupLoadingStatus = {
        ...this.createGroupLoadingStatus,
        ...status,
      };
    },

    SET_LOADING_CSV_GROUP_ID(id: number | string | null) {
      this.CSVLoadingGroupId = id;
    },

    SET_CSV_LOADING_STATUS(status: TLoadingStatus) {
      this.getCSVLoadingStatus = {
        ...this.getCSVLoadingStatus,
        ...status,
      };
    },

    SET_CSV_FILE(file: string | null) {
      this.csvFile = file;
    },
  },
});
