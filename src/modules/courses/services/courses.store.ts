import { defineStore } from 'pinia';
import { IAdminCourseDto, ICourseShortDto } from 'src/modules/courses/types/courses.types';
import { LoadingStatusActionsEnum, LoadingStatusCodesEnum, TLoadingStatus } from 'src/types/base.types';
import { IGroupStudent, IUserInfo } from 'src/modules/courses/types/entity.types';
import { IGroupDto } from 'src/modules/groups/types/groups.types';

interface ICoursesState {
  courses: ICourseShortDto[] | null;
  coursesLoadingStatus: TLoadingStatus;

  course: ICourseShortDto | null;
  courseLoadingStatus: TLoadingStatus;

  adminCourses: IAdminCourseDto[] | null;
  adminCoursesLoadingStatus: TLoadingStatus;

  courseStudents: IUserInfo[] | null;
  courseStudentsLoadingStatus: TLoadingStatus;

  courseGroups: IGroupDto[] | null;
  courseGroupsLoadingStatus: TLoadingStatus;

  adminGroupData: IGroupStudent[] | null;
  adminGroupDataLoadingStatus: TLoadingStatus;

  outGroupList: IGroupDto[] | null;
  outGroupListLoadingStatus: TLoadingStatus;

  allocateLoadingStatus: TLoadingStatus;

  taskFileLoadingStatus: TLoadingStatus;
  createTasksLoadingStatus: TLoadingStatus;
  patchTaskLoadingStatus: TLoadingStatus;

  createCourseLoadingStatus: TLoadingStatus;

  CSVLoadingStatus: TLoadingStatus;
}

export const coursesStore = defineStore({
  id: 'courses',

  state: (): ICoursesState => ({
    // ----------------------------------------------------------------
    // Данные
    courses: null,
    course: null,

    adminCourses: null,

    courseStudents: null,
    courseGroups: null,

    adminGroupData: null,

    outGroupList: null,

    // ----------------------------------------------------------------
    // Loading статусы
    coursesLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    courseLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    adminCoursesLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    courseStudentsLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    courseGroupsLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    adminGroupDataLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    outGroupListLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    allocateLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    taskFileLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    createTasksLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    patchTaskLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    createCourseLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },

    CSVLoadingStatus: {
      code: LoadingStatusCodesEnum.notLoaded,
      action: LoadingStatusActionsEnum.noAction,
      errorCode: '',
      msg: '',
    },
  }),

  actions: {
    // ----------------------------------------------------------------
    // Установка данных в стор
    SET_COURSES_PAYLOAD(payload: ICourseShortDto[] | null) {
      this.courses = payload;
    },

    SET_COURSE_PAYLOAD(payload: ICourseShortDto | null) {
      this.course = payload;
    },

    SET_ADMIN_COURSES_PAYLOAD(payload: IAdminCourseDto[] | null) {
      this.adminCourses = payload;
    },

    SET_COURSE_GROUPS_PAYLOAD(payload: IGroupDto[] | null) {
      this.courseGroups = payload;
    },

    SET_COURSE_STUDENTS_PAYLOAD(payload: IUserInfo[] | null) {
      this.courseStudents = payload;
    },

    SET_ADMIN_GROUP_DATA_PAYLOAD(payload: IGroupStudent[] | null) {
      this.adminGroupData = payload;
    },

    SET_OUT_GROUP_LIST_PAYLOAD(payload: IGroupDto[] | null) {
      this.outGroupList = payload;
    },

    // ----------------------------------------------------------------
    // Установка лоадинг статусов
    SET_COURSES_LOADING_STATUS(status: TLoadingStatus) {
      this.coursesLoadingStatus = {
        ...this.coursesLoadingStatus,
        ...status,
      };
    },

    SET_COURSE_LOADING_STATUS(status: TLoadingStatus) {
      this.courseLoadingStatus = {
        ...this.courseLoadingStatus,
        ...status,
      };
    },

    SET_ADMIN_COURSES_LOADING_STATUS(status: TLoadingStatus) {
      this.adminCoursesLoadingStatus = {
        ...this.adminCoursesLoadingStatus,
        ...status,
      };
    },

    SET_COURSE_GROUPS_LOADING_STATUS(status: TLoadingStatus) {
      this.courseGroupsLoadingStatus = {
        ...this.courseGroupsLoadingStatus,
        ...status,
      };
    },

    SET_COURSE_STUDENTS_LOADING_STATUS(status: TLoadingStatus) {
      this.courseStudentsLoadingStatus = {
        ...this.courseStudentsLoadingStatus,
        ...status,
      };
    },

    SET_ADMIN_GROUP_DATA_LOADING_STATUS(status: TLoadingStatus) {
      this.adminGroupDataLoadingStatus = {
        ...this.adminGroupDataLoadingStatus,
        ...status,
      };
    },

    SET_OUT_GROUP_LIST_LOADING_STATUS(status: TLoadingStatus) {
      this.outGroupListLoadingStatus = {
        ...this.outGroupListLoadingStatus,
        ...status,
      };
    },

    SET_ALLOCATE_LOADING_STATUS(status: TLoadingStatus) {
      this.allocateLoadingStatus = {
        ...this.allocateLoadingStatus,
        ...status,
      };
    },

    SET_TASK_FILE_LOADING_STATUS(status: TLoadingStatus) {
      this.taskFileLoadingStatus = {
        ...this.taskFileLoadingStatus,
        ...status,
      };
    },

    SET_CREATE_TASKS_LOADING_STATUS(status: TLoadingStatus) {
      this.createTasksLoadingStatus = {
        ...this.createTasksLoadingStatus,
        ...status,
      };
    },

    SET_PATCH_TASK_LOADING_STATUS(status: TLoadingStatus) {
      this.patchTaskLoadingStatus = {
        ...this.patchTaskLoadingStatus,
        ...status,
      };
    },

    SET_CREATE_COURSE_LOADING_STATUS(status: TLoadingStatus) {
      this.createCourseLoadingStatus = {
        ...this.createCourseLoadingStatus,
        ...status,
      };
    },

    SET_CSV_LOADING_STATUS(status: TLoadingStatus) {
      this.CSVLoadingStatus = {
        ...this.CSVLoadingStatus,
        ...status,
      };
    },
  },
});
