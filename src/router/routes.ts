import { RouteRecordRaw } from 'vue-router';
import TasksPage from 'pages/student/TasksPage.vue';
import CoursesPage from 'pages/student/CoursesPage.vue';
import TaskPage from 'pages/student/TaskPage.vue';
import MainLayout from 'layouts/MainLayout.vue';
import AdminLayout from 'layouts/AdminLayout.vue';
import AdminCourses from 'pages/admin/courses/CoursesPage.vue';
import AdminCourse from 'pages/admin/courses/CoursePage.vue';
import EntityList from 'pages/admin/courses/EntityList.vue';
import AddEntity from 'pages/admin/courses/AddEntity.vue';
import EntityTaskList from 'pages/admin/courses/EntityTaskList.vue';
import CreateGroup from 'pages/admin/courses/CreateGroup.vue';
import CreateCourse from 'pages/admin/courses/CreateCourse.vue';
import UserPage from 'pages/admin/courses/UserPage.vue';
import AuthLayout from 'layouts/AuthLayout.vue';
import LoginPage from 'pages/auth/LoginPage.vue';
import EntityPage from 'pages/admin/courses/EntityPage.vue';
import AdminTask from 'pages/admin/courses/AdminTask.vue';
import AdminGroups from 'pages/admin/groups/AdminGroups.vue';
import ErrorNotFound from 'pages/ErrorNotFound.vue';
import AddCourseEntity from 'pages/admin/courses/AddCourseEntity.vue';
import AdminGroupCreate from 'pages/admin/groups/AdminGroupCreate.vue';
import EditPage from 'pages/admin/tasks/EditPage.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/student/',
    component: MainLayout,
    children: [
      {
        path: '',
        name: 'Courses',
        component: CoursesPage,
      },
      {
        path: 'course/:courseId',
        name: 'Course',
        component: TasksPage,
      },
      {
        path: 'task/:taskId',
        name: 'Task',
        component: TaskPage,
      },
      /* todo: раскомментировать после замены редактора
      {
        path: 'task/:taskId/edit',
        name: 'TaskEditor',
        component: EditorPage,
      }, */
    ],
  },

  {
    path: '/admin/',
    component: AdminLayout,
    children: [
      // -------------------------------------------------------------
      // Страницы работы с курсами
      {
        path: '',
        name: 'AdminCourses',
        component: AdminCourses,
      },
      {
        path: 'course/:courseId',
        name: 'AdminCourse',
        component: AdminCourse,
      },
      {
        path: 'course/:courseId/entities',
        name: 'EntityList',
        component: EntityList,
      },
      {
        path: 'course/:courseId/entity/add',
        name: 'AddEntity',
        component: AddEntity,
      },
      {
        path: 'course/:courseId/:entityId/tasks',
        name: 'EntityTasks',
        component: EntityTaskList,
      },
      {
        path: 'task/:courseId/create',
        name: 'EditPage',
        component: EditPage,
      },
      {
        path: 'group/create',
        name: 'CreateGroup',
        component: CreateGroup,
      },
      {
        path: 'course/create',
        name: 'CreateCourse',
        component: CreateCourse,
      },
      {
        path: 'course/:courseId/groups/:entityId',
        name: 'EntityPage',
        component: EntityPage,
      },
      {
        path: 'course/:courseId/groups/user/:userId',
        name: 'UserPage',
        component: UserPage,
      },
      {
        path: 'courses/:courseId/users/:userId/tasks/:taskId',
        name: 'AdminTask',
        component: AdminTask,
      },
      {
        path: 'courses/:courseId/groups/:groupId/add',
        name: 'AddCourseEntity',
        component: AddCourseEntity,
      },

      // -------------------------------------------------------------
      // Страницы для работы с группами
      {
        path: 'groups',
        name: 'AdminGroups',
        component: AdminGroups,
      },
      {
        path: 'groups/create',
        name: 'AdminGroupCreate',
        component: AdminGroupCreate,
      },
    ],
  },

  {
    path: '/auth/',
    component: AuthLayout,
    children: [
      {
        path: 'signing',
        name: 'Signing',
        component: LoginPage,
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: ErrorNotFound,
  },
];

export default routes;
