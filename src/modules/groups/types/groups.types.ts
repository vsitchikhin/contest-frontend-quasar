export interface IGroupDto {
  id: number;
  name: string;
  count_users: number;
}

export interface ICreateGroupDto {
  name: string;
  count_generate: number;
}
